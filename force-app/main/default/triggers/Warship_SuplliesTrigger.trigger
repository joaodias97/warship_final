trigger Warship_SuplliesTrigger on Warship_Suplly__c (after insert, after update) {

Set<Id> setResourceId = new set<Id>();
	Set<Id> setWarshipId = new set<Id>();
	
    for (Warship_Suplly__c r : trigger.new) {
		
		if(r.Suplly__c != null)
		{
			setResourceId.add(r.Suplly__c);
		}
		if(r.Suplly__c != null)
		{
			setWarshipId.add(r.warship__c);
		}
	}
	
	List<Warship_Suplly__c> lstRW =  [ SELECT id,Suplly__c,Warship__c FROM Warship_Suplly__c WHERE  Suplly__c = :setResourceId  AND Warship__c = :setWarshipId ];
	Map<String,Warship_Suplly__c> mapRW = new Map<String,Warship_Suplly__c>();
	for(Warship_Suplly__c RW : lstRW)
	{
		String key = RW.Suplly__c+'_'+RW.Warship__c;
		mapRW.put(key,RW);
	}
	
	
    for (Warship_Suplly__c r : trigger.new) {
		String key = r.Suplly__c+'_'+r.Warship__c;
        if ( mapRW.containsKey(key) ){
			r.addError('This supply already has this Supply');
        }
	}
}