trigger Warship_ResourcesTrigger on Warship_Resources__c (after insert, after update) {
	
    	Set<Id> warship_resourcesErrors = new Set<Id>();
	for(Warship_Resources__c r : Trigger.new){
        Set<String> resourceNames = new Set<String>();

		for(Warship_Resources__c s : [SELECT Id, Name FROM Warship_Resources__c WHERE Name =:r.Name]){
			 resourceNames.add(s.Name);
                if(!resourceNames.add(s.Name)) 
                warship_resourcesErrors.add(s.Id);
		}
	}

	if(!warship_resourcesErrors.isEmpty()){
        Integer result=warship_resourcesErrors.size();
		for(Warship_Resources__c r : Trigger.new){
            if(warship_resourcesErrors.contains(r.Id)) {
                if(result>1){  
                	r.addError('This warship resource already has a '+r.Name);
				}
			}
		}
    }
}