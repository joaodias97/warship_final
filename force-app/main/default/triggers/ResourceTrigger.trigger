trigger ResourceTrigger on Warship_Resources__c (before insert, before update) {
    Set<Id> setResourceId = new set<Id>();
	Set<Id> setWarshipId = new set<Id>();
	
    for (Warship_Resources__c r : trigger.new) {
		
		if(r.Resource__c != null)
		{
			setResourceId.add(r.Resource__c);
		}
		if(r.Warship__c != null)
		{
			setWarshipId.add(r.warship__c);
		}
	}
	
	List<Warship_Resources__c> lstRW =  [ SELECT id,Resource__c,Warship__c FROM Warship_Resources__c WHERE  Resource__c = :setResourceId  AND Warship__c = :setWarshipId ];
	Map<String,Warship_Resources__c> mapRW = new Map<String,Warship_Resources__c>();
	for(Warship_Resources__c RW : lstRW)
	{
		String key = RW.Resource__c+'_'+RW.Warship__c;
		mapRW.put(key,RW);
	}
	
	
    for (Warship_Resources__c r : trigger.new) {
		String key = r.Resource__c+'_'+r.Warship__c;
        if ( mapRW.containsKey(key) ){
			r.addError('This warship already has this Resource');
        }
	}
}