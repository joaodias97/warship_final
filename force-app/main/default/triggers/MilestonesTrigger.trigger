trigger MilestonesTrigger on Warship__c (before insert, before update) {

    for(warship__c a : Trigger.New) {

        
              warship__c [] war = [SELECT Milestons__c,Project_Status__c, Name FROM warship__c];
              
        if(a.Milestons__c == 0) {
            a.Project_Status__c = 'Green';
        }else if(a.Milestons__c == 1) {
            a.Project_Status__c = 'Yellow';
        }else if(a.Milestons__c >= 2) {
            a.Project_Status__c = 'Red';

       }   
    }   
}