trigger EndDateTrigger on Warship__c (before insert, before update) {
    
    for(warship__c a : Trigger.New) {
    	warship__c [] war = [SELECT End_Date__c,Project_Status__c, Name FROM warship__c];
       
        Date d1 = Date.today();
        
        if(d1 < a.End_Date__c ) {
            a.Project_Status__c = 'Green';
        }else{
            a.Project_Status__c = 'Red';
        }   
    }
}