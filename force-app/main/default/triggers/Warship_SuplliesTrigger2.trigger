trigger Warship_SuplliesTrigger2 on Warship_Suplly__c (after insert, after update) {

	Set<Id> warship_suplliesErrors = new Set<Id>();
	for(Warship_Suplly__c r : Trigger.new){
        Set<String> resourceNames = new Set<String>();
		for(Warship_Suplly__c s : [SELECT Id, Name FROM Warship_Suplly__c WHERE Name =:r.Name]){
			 resourceNames.add(s.Name);
                if(!resourceNames.add(s.Name)) 
               warship_suplliesErrors.add(s.Id);
		}
	}

	if(!warship_suplliesErrors.isEmpty()){
        Integer result=warship_suplliesErrors.size();
		for(Warship_Suplly__c r : Trigger.new){
            if(warship_suplliesErrors.contains(r.Id)) {
                if(result>1){    
       				r.addError('This warship resource already has a '+r.Name);
				}
			}
		}
    }
}