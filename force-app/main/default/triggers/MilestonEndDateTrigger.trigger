trigger MilestonEndDateTrigger on Warship__c (before insert, before update) {

    for(warship__c a : Trigger.New) {
    	warship__c [] war = [SELECT Milestons__c, End_Date__c, Name FROM warship__c];
       
        Date d1 = Date.today();
        
        if(d1 < a.End_Date__c ) {
            a.Milestons__c = 0;
        }else if(d1 == a.End_Date__c){
            a.Milestons__c = 1;
        }else if(d1 >  a.End_Date__c){
            a.Milestons__c = 2;
    	}
    }
}