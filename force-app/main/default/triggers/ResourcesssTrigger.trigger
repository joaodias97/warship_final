trigger ResourcesssTrigger on Resource__c (after insert, after update) {

	Set<Id> warship_resourcesErrors = new Set<Id>();
	for(Resource__c r : Trigger.new){
        Set<String> resourceNames = new Set<String>();

		for(Resource__c s : [SELECT Id, Name FROM Resource__c WHERE Name =:r.Name]){
			 resourceNames.add(s.Name);
                if(!resourceNames.add(s.Name)) 
                warship_resourcesErrors.add(s.Id);
		}
	}

	if(!warship_resourcesErrors.isEmpty()){
        Integer result=warship_resourcesErrors.size();
		for(Resource__c r : Trigger.new){
            if(warship_resourcesErrors.contains(r.Id)) {
                if(result>1){  
                	r.addError('This Resource already has a '+r.Name);
				}
			}
		}
    }
}