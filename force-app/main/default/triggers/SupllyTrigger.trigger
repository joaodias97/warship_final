trigger SupllyTrigger on Suplly__c (after insert, after update) {
  Set<Id> warship_supliesErrors = new Set<Id>();
	for(Suplly__c r : Trigger.new){
        Set<String> resourceNames = new Set<String>();

		for(Suplly__c s : [SELECT Id, Name FROM Suplly__c WHERE Name =:r.Name]){
			 resourceNames.add(s.Name);
                if(!resourceNames.add(s.Name)) 
                warship_supliesErrors.add(s.Id);
		}
	}

	if(!warship_supliesErrors.isEmpty()){
        Integer result=warship_supliesErrors.size();
		for(Suplly__c r : Trigger.new){
            if(warship_supliesErrors.contains(r.Id)) {
                if(result>1){   
                    r.addError('This Supply already has a '+r.Name);
		        }
	        }
        }
    }
}